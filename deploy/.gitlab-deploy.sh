#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVERS
array=(${string//,/ })

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
  echo "Deploy project on server ${array[i]}"

  # Deploy a docker stack
  scp ./deploy/docker-compose.yml $CI_DEPLOY_USER@${array[i]}:~
  ssh $CI_DEPLOY_USER@${array[i]} "sudo docker stack deploy -c docker-compose.yml $CI_PROJECT_NAME"
  # keep the docker-compose.yml intentionally

  # Remove stopped containers and danggling images
  ssh $CI_DEPLOY_USER@${array[i]} "sudo docker system prune -f"
done
