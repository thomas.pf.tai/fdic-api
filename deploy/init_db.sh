#!/bin/bash
# Create a database ‘fdic_institutions2’
curl -X PUT http://couchdbadmin:couchdbadmin@localhost:5984/fdic_institutions2
# Download the FDIC Institutions data file 
mkdir data
cd data
curl -O https://raw.githubusercontent.com/remkohdev/fdic-api/master/data/INSTITUTIONS2.json
cd ..
# Bulk insert to populate the CouchDB
curl -X POST -H "Content-Type: application/json" "http://couchdbadmin:couchdbadmin@localhost:5984/fdic_institutions2/_bulk_docs" -d '@data/INSTITUTIONS2.json'
# Create views in CouchDB
curl -X PUT -H "Content-Type: application/json" -d '{"views":{"name-view":{"map":"function(doc){ if(doc.NAME){ emit(doc.NAME, doc); }}"}},"language":"javascript"}' http://couchdbadmin:couchdbadmin@localhost:5984/fdic_institutions2/_design/name
curl -X PUT -H "Content-Type: application/json" -d '{"views":{"asset-view":{"map":"function(doc){ if(doc.ASSET){ emit(parseInt(doc.ASSET.replace(/,/g, \"\")), doc); }}"}},"language":"javascript"}' http://couchdbadmin:couchdbadmin@localhost:5984/fdic_institutions2/_design/asset
curl -X PUT -H "Content-Type: application/json" -d '{"views":{"name-asset-view":{"map":"function(doc){ if(doc.NAME && doc.ASSET){ emit(doc.NAME, doc.ASSET); }}"}},"language":"javascript"}' http://couchdbadmin:couchdbadmin@localhost:5984/fdic_institutions2/_design/name-asset
curl -X PUT -H "Content-Type: application/json" -d '{"views":{"fed-rssd-view":{"map":"function(doc){ if(doc.FED_RSSD){ emit(doc.FED_RSSD); }}"}},"language":"javascript"}' http://couchdbadmin:couchdbadmin@localhost:5984/fdic_institutions2/_design/fed-rssd